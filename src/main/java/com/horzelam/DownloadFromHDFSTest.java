package com.horzelam;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;

import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

/**
 * Various tool scripts to operate on hdfs data.
 */
public class DownloadFromHDFSTest {


    private static URI fsURI = URI.create("hdfs://my.node.on.hadoop.cluster.com:8020/");
    private static final Configuration conf = new Configuration();

    public static void main(String[] args) throws IOException {
        System.setProperty("HADOOP_USER_NAME", "myusername");
        //dump(new Path("/user/horzelam/somedir/names/"), new Path("file:///tmp/names/"));
        //list(new Path("/user/horzelam/somedir/names/"));


        // delete empty AVRO files - with given format empty file has this particular size
        //final int EMPTY_AVRO_FILESIZE = 268;
        //deleteFilesForGivenSize(new Path("/user/horzelam/somedir/names/"), EMPTY_AVRO_FILESIZE);

        removeEmptyDirs(new Path("/user/horzelam/mappings/keyword/"));
    }

    private static void removeEmptyDirs(final Path path) throws IOException {
        System.out.println("List empty dirs on: " + path);
        try(FileSystem fs = FileSystem.get(fsURI, conf)) {
            final FileStatus[] files = fs.listStatus(path);
            for(FileStatus fileStatus : files) {
                if(fileStatus.isDirectory()) {
                    final RemoteIterator<LocatedFileStatus> subFiles = fs.listFiles(fileStatus.getPath(), true);
                    if(subFiles.hasNext()) {
                        System.out.println("Dir: " + fileStatus.getPath() + " is not empty");
                    } else {
                        System.out.println("Dir: " + fileStatus.getPath() + " is empty - deleting the folder");
                        fs.delete(fileStatus.getPath(), false);
                    }
                }
            }
        }
    }

    public static void deleteFilesForGivenSize(final Path path, int sizeOfFileToDelete) throws IOException {
        System.out.println("Deleting files below given size on: " + path);
        try(FileSystem fs = FileSystem.get(fsURI, conf)) {
            final RemoteIterator<LocatedFileStatus> files = fs.listFiles(path, true);
            Set<Path> toDelete = new HashSet<>();
            while(files.hasNext()) {
                final LocatedFileStatus file = files.next();
                if(file.getLen() == sizeOfFileToDelete) {
                    toDelete.add(file.getPath());
                } else {
                    System.out.println("Left : " + file.getPath() + " - " + file.getLen());
                }
            }
            System.out.println("Amount of files to delete: " + toDelete.size());
            for(Path fileToDelete : toDelete) {
                fs.delete(fileToDelete, false);
            }
        }
    }

    public static void list(final Path path) throws IOException {
        System.out.println("Listing content of " + path);
        try(FileSystem fs = FileSystem.get(fsURI, conf)) {
            final RemoteIterator<LocatedFileStatus> files = fs.listFiles(path, true);
            while(files.hasNext()) {
                final LocatedFileStatus file = files.next();
                System.out.println(file.getPath() + " - " + file.getLen());
            }
        }
    }

    public static void dump(Path src, Path dest) throws IOException {
        System.out.println("Downloading src: " + src + " to " + dest);
        FileSystem fs = FileSystem.get(fsURI, conf);
        if(fs.exists(src)) {
            fs.copyToLocalFile(src, dest);
        }
        fs.close();
    }
}
